JC = javac

all:
	$(JC) Antivirus.java
	java Antivirus 

antivirus: coordinate actor sectors ascii
	$(JC) Antivirus.java  

# Etapas

coordinate: Coordinate.java
	$(JC) Coordinate.java 

actor: Actor.java Enemy.java Player.java Attack.java Support.java coordinate
	$(JC) Actor.java Enemy.java Player.java Attack.java Support.java

sectors: Sector.java PublicSec.java ProtectedSec.java PrivateSec.java SectorMap.java coordinate
	$(JC) Sector.java PublicSec.java ProtectedSec.java PrivateSec.java SectorMap.java

ascii: AsciiMap.java Igrafica.java actor sectors
	$(JC) Igrafica.java AsciiMap.java

# Limpeza

clean:
	rm -f *.class;
 
purge: clean
	rm -f Antivirus
