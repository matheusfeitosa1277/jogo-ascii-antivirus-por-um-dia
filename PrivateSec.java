/**
 * Setor Privado.
 *
 * C. Setor privado: A restrição é que os jogadores não podem executar a ação
 * de procurar no setor
 */
public class PrivateSec extends Sector {
    /* Construtores */
    public PrivateSec(int wallPattern) {
        super(wallPattern);
    }
}
