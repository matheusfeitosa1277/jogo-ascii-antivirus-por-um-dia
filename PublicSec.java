/**
 * Setor Normal.
 *
 * A. Setor normal: Não existe nenhuma restrição, ou seja, todas as ações dos
 * jogadores P1 e P2 podem ser executadas. Os inimigos sempre recebem o
 * dano de um ataque e a procura pode ser efetuada normalmente.
 */

public class PublicSec extends Sector {
    /* Construtores */
    public PublicSec(int wallPattern) {
        super(wallPattern);
    }
}
