import java.util.Random;
import java.util.ArrayList;

/**
 * Setor Oculto.
 *
 * B. Setor oculto: A restrição neste setor é que quando o jogador efetuar um
 * ataque, o vírus pode ou não ser eliminado (aleatório), pois o jogador não
 * sabe a localização exata do vírus no setor.
 */
public class ProtectedSec extends Sector {
    /* Construtores */
    public ProtectedSec(int wallPattern) {
        super(wallPattern);
    }

    @Override
    public void defend(int atk) {
        Random dice = new Random();

        int evade = dice.nextInt(2);

        if (evade == 1) {
            ArrayList<Enemy> infection = this.mobs;

            if (!infection.isEmpty()) {
                Enemy aux = infection.get(0);        
                aux.setDef(aux.getDef() - atk);

                if (aux.getDef() == 0)  //Inimigo foi Morto
                    infection.remove(0);
            }
        }
    }
}

