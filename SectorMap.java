import java.util.Random;
import java.util.Scanner;

import java.nio.file.Paths;

/**
 * O cenário do jogo é formado por um tabuleiro 5x5, conforme ilustrado na Figura 1.
 *
 * Abaixo seguem as informações sobre o tabuleiro mostrado na Figura 1
 *
 * 1. Os jogadores iniciam o jogo no centro do tabuleiro (posição C).
 *
 * 2. A fonte de infecção de vírus (posição X), que consiste no objetivo final, deve ser
 * aleatoriamente posicionada em qualquer posição do tabuleiro que não seja a central.
 *
 * 3. Cada posição do tabuleiro corresponde a um setor que o jogador deve percorrer, até
 * encontrar a fonte de infecção do vírus (X).
 *
 * 7. Os limites do tabuleiro (parte externa) são sempre paredes, ou seja, não podem
 * possuir portas.
 *
 * 8. A posição inicial (C) sempre possui as 4 portas abertas, que estão representadas na
 * Figura 1 por um asterisco (*).
 *
 */

public class SectorMap {
    /* Atributos */
    private static String DEFAULT_MAZE = "./resources/defaultMaze";
    public static final int DEFAULT_SIZE = 5, MID = 2; //[0..4]

    private Sector map[][];//[0..4][0..4]
    private Coordinate infection;

    /* Construtores */
    public SectorMap() {
        this.setMap();  //Aloca memoria
        this.defaultSectorMap(); //Inicializa
    }

    /* Getters and Setters */
    public Sector getSec(Coordinate frame) {
        return this.map[frame.getX()][frame.getY()];
    }

    public Sector[][] getMap() { return this.map; }

    public Coordinate getInfection() { return this.infection; }

    public void setMap(Sector map[][]) { this.map = map; }

    public void setMap() { this.map = new Sector[DEFAULT_SIZE][DEFAULT_SIZE]; }

    public void setMap(Coordinate infection) { this.infection = infection; }

    /* Metodos */
    private void defaultSectorMap() { 
        this.defaultMaze();
        this.startEnemies();
    }

    private void defaultMaze() {
        try {
            int m, n, wall;
            Sector aux;

            String type = new String();
            Random dice = new Random();
            Scanner reader = new Scanner(Paths.get(DEFAULT_MAZE)); 

            for (n = 0; n < DEFAULT_SIZE; n++) 
                for (m = 0; m < DEFAULT_SIZE; m++) {
                    wall = reader.nextInt();

                    switch(dice.nextInt(3)) { // [0..2] - Function Pointer Seria interesante
                        case 0: aux = new PublicSec(wall); break;
                        case 1: aux = new ProtectedSec(wall); break;
                        case 2: aux = new PrivateSec(wall); break;
                        default: aux = new PublicSec(wall); break; // Desnecessario, mas o compilador me obriga...
                    }

                    this.map[m][n] = aux;
                }

           this.map[MID][MID] = new PublicSec(1); // Sobrescreve Setor Central
           this.infection = new Coordinate(3, 3); // Cria centro da infeccao

        } catch (Exception e) { System.out.println(e); }
    }

    private void startEnemies() {
        int m, n;
        Random dice = new Random();

        for (n = 0; n < DEFAULT_SIZE; n++) 
            for (m = 0; m < DEFAULT_SIZE; m++)
                this.map[m][n].generateEnemies();

        this.map[MID][MID].clearEnemys(); this.map[3][3].clearEnemys();
    }

        /* Debug */
    public void drawRaw() {
        int m, n;
        Sector map[][] = this.getMap();

        for (n = 0; n < DEFAULT_SIZE; n++) {
            for (m = 0; m < DEFAULT_SIZE; m++) 
                System.out.printf("%3d ", map[m][n].getWallPattern());
            System.out.println();
        }
    }
}

