import java.util.Random;

/**
 * Os inimigos são os vírus de computador gerados por uma fonte de infecção.
 *
 * O objetivo do jogo é que um dos jogadores, apresentados na Seção 2.1, encontre essa fonte de infecção.
 *
 * 1. Cada inimigo possui um valor para ataque (ATK) e um valor de defesa (DEF), sendo
 * que o poder de ataque e defesa devem ser inicialmente iguais, com valores entre 1 e 3.
 * Por exemplo, um inimigo pode ter ATK=1 e DEF=1 (representado por 1/1), e outro
 * pode ter ATK=2 e DEF=2 (representado por 2/2).
 *
 * 2. --- 
 *
 * 3. Os valores de ATK e DEF devem ser definidos aleatoriamente, no momento em que
 * os inimigos forem criados.
 *
 * 4. Os inimigos só podem executar a ação de atacar
 * 
 * 5. Os inimigos não se movimentam, não efetuam procuras ou recuperam defesa
 */

public class Enemy extends Actor {
    /* Atributos */
    private static final int MAX_STAT = 3;

    /* Construtores */
    public Enemy() {
        Random dice = new Random();
        int rand;

        rand = dice.nextInt(MAX_STAT) + 1; //[1..Max_STAT]

        this.setAtk(rand); this.setDef(rand);
    }

    /* Metodos */
    public void attack(Player antivirus) {
        Random dice = new Random();
        
        if (dice.nextInt(2) == 0) //Se for par
            this.attack((Actor) antivirus);
    }
}

