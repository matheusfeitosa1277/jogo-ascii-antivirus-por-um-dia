Para avaliação do trabalho, o sistema deverá atender aos conceitos da orientação à objetos
listados abaixo:

1. Classes (atributos e métodos)
2. Construtores
3. Encapsulamento
4. Herança
5. Interface
6. Classe Abstrata
7. Polimorfismo
8. Coleção
