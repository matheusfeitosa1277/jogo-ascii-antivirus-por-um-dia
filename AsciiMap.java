import java.util.Scanner;
import java.util.Iterator;
import java.nio.file.Paths;

/**
 * Classe responsavel pela implementacao da interface grafica do programa.
 */

public class AsciiMap implements Igrafica {
    /* Atributos */
    private static String DEFAULT_MAP= "./resources/defaultTable";
    private static int I = 64, J = 18;

    private char map[][];

    /* Construtores */
    public AsciiMap() {
        this.defaultAsciiMap();
    }

    /* Getters and Setters */
    public char[][] getMap() { return this.map; }

    public void setMap(char map[][]) { this.map = map; }

    /* Metodos */
    private void defaultAsciiMap() {
        int i, j = 0;
        String line;

        this.map = new char[I][J];

        try { 
            Scanner reader = new Scanner(Paths.get(DEFAULT_MAP));
            
            while (reader.hasNextLine()) {
                line = reader.nextLine();

                for (i = 0; i < line.length(); i++) 
                    this.map[i][j] = line.charAt(i);

                j++;
            }
        } catch (Exception e) { System.out.println(e); }
    }

        /* debug */
    public void revealMap(SectorMap secMap) {
        int i, j;

        Coordinate position;
        Sector aux[][] = secMap.getMap();

        for (j = 0; j < secMap.DEFAULT_SIZE; j++) 
            for (i = 0; i < secMap.DEFAULT_SIZE; i++) {
                position = new Coordinate(i, j);
                this.revealSector(aux[i][j], position);
            }
    }

        /* Metodos Pre Combat */
            /* Escrita Mapa */
    public void revealSector(SectorMap secMap, Coordinate pos) {
        revealSector(secMap.getSec(pos), pos);
    }

    public void revealSector(Sector sec, Coordinate pos) { 
        int x, y, wall = sec.getWallPattern();
        char aux[][] = this.getMap(); //Ponteiro pro mapa

        x = 5 + 4*pos.getX(); y = 7 + 2*pos.getY(); //Posicao relativa na moldura

        if ((wall % 2) != 0)    //Baixo
            aux[x][y+1] = '*';

        if ((wall % 3) != 0)    //Direita
            aux[x+2][y] = '*';

        if ((wall % 5) != 0) 
            aux[x][y-1] = '*';  //Cima

        if ((wall % 7) != 0) 
            aux[x-2][y] = '*';  //Esq
    }

    /**
     * Limpa a posicao do player no mapa.
     */

    public void cleanPosition(Player antivirus) {
        int x, y;
        char aux[][] = this.getMap(); //Ponteiro pro mapa
        Coordinate pos = antivirus.getPosition();

        x = 5 + 4*pos.getX(); y = 7 + 2*pos.getY(); //Posicao relativa na moldura

        switch (aux[x][y]) {
            case 'C': return;
            case '1': case '2': aux[x][y] = ' '; break;
            case 'P':
                if (antivirus instanceof Attack)
                    aux[x][y] = '2';
                else aux[x][y] = '1';
                break;
        }
    }

    /**
     * Altera a posicao do player no mapa.
     */

    public void updatePosition(Player antivirus) {
        int x, y;
        char aux[][] = this.getMap(); //Ponteiro pro mapa
        Coordinate pos = antivirus.getPosition();

        x = 5 + 4*pos.getX(); y = 7 + 2*pos.getY(); //Posicao relativa na moldura
        
        switch (aux[x][y]) {
            case 'C': return;
            case '1': case '2': aux[x][y] = 'P'; return;
        }

        if (antivirus instanceof Attack) 
            aux[x][y] = '1';
        else aux[x][y] = '2';
    }
            /* Escrita Minimapa */
   /**
    * Altera a posicao do Player no minimapa.
    */

    public void updateMiniPos(Player antivirus) {
        int aux = 37;
        Coordinate pos = antivirus.getPosition();
        char x, y; 

        x = (char) (pos.getX() + '0'); y = (char) (pos.getY() + '0');

        if (antivirus instanceof Support) aux += 18;
        
        this.map[aux][7] = x; aux+= 3; this.map[aux][7] = y;
    }

        /* Metodos Combat */ 
    /**
     * Altera os valores de atk e def dos jogadores no minimapa.
     */

    public void updateMiniHeroes(Player heroes[]) {
        for (int i = 0; i < 2; i++)
            updateMiniStats(heroes[i]);
    }

    public void updateMiniStats(Player antivirus) {
        int aux = 31;
        char atk, def;

        atk = (char) (antivirus.getAtk() + '0'); def = (char) (antivirus.getDef() + '0');

        if (antivirus instanceof Support) aux += 25;

        this.map[aux][14] = atk; aux+= 2; this.map[aux][14] = def;
    }

    /**
     * Altera os valores de atk e def dos inimigos no minimapa.
     *
     * Remove inimigos mortos do minimapa durante a rodada
     */

    public void updateHeroes(Player heroes[], SectorMap secMap) {
        Player aux;

        for (int i = 0; i < 2; i++) {
            aux = heroes[i];
            this.updateEnemys(aux, secMap.getSec(aux.getPosition())); 
        }
    }

    public void updateEnemys(Player antivirus, Sector sec) {
        Iterator<Enemy> infection = sec.getMobs().iterator();
        Enemy virus;
        char atk, def;
        int aux = 30;

        if (antivirus instanceof Support) aux += 18;

        /* Limpa o buffer */
        for (int i = aux; i < aux + 10; i++) 
            this.map[i][10] = ' ';

        /* Escreve inimigos */
        while (infection.hasNext()) {
            virus = infection.next();
            atk = (char) (virus.getAtk() + '0'); def = (char) (virus.getDef() + '0');
            this.map[aux][10] = atk; this.map[aux+1][10] = '/'; this.map[aux+2][10] = def;
            aux += 4;
        }
    }

    /* Implementacao da Igrafica */
    public void cleanCanvas() {
        System.out.print("\033[H\033[2J");
    }

    public void drawMap() {
        int i, j;

        for (j = 0; j < J; j++) {
            for (i = 0; i < I; i++)
                System.out.print(this.map[i][j]);
           System.out.println();
        }

    }

    public void drawMoveMenu(Player antivirus) {
        int p = (antivirus instanceof Attack ? 1 : 2);
        
        System.out.printf("Where to go PLAYER %d (P%d)?\n", p, p); 

        System.out.println("   U - Up");
        System.out.println("   D - Down");
        System.out.println("   L - Left");
        System.out.println("   R - Right");
        System.out.print("\n R: ");
    }

    public void drawActionMenu(Player antivirus, Sector sec) {
        int p = (antivirus instanceof Attack ? 1 : 2);
        
        System.out.printf("Action PLAYER %d (P%d)? | %s\n", p, p, sec.getClass().getSimpleName()); 

        if (sec.checkCombat())              System.out.println("   A - Attack");  
        if (!(sec instanceof PrivateSec))   System.out.println("   S - Search");  
        if (p == 2)                         System.out.println("   H - Heal");  
        System.out.println("   N - Nothing (default)");

        System.out.print("\n R: ");
    }

    public void drawHealMenu(Player heroes[]) {
        System.out.printf("\nWho?\n");
        if (heroes[0].getPosition().equal(heroes[1].getPosition()))
            System.out.println("    A - P1");

        System.out.println("    S - P2");
        System.out.print("\n R: ");
    }

    public void drawInfection(Player antivirus) {
        int x, y;
        char aux[][] = this.getMap(); //Ponteiro pro mapa
        Coordinate pos = antivirus.getPosition();

        x = 5 + 4*pos.getX(); y = 7 + 2*pos.getY(); //Posicao relativa na moldura

        aux[x][y] = 'X';
    }

    public void drawVictory() {
        System.out.println("################################");
        System.out.println("### You have won the game!!! ###");
        System.out.println("################################");
    }

    public void drawLost() {
        System.out.println("################################");
        System.out.println("###     You have Lost :(     ###");
        System.out.println("################################");
    }
}

