/**
 * Superclasse que representa as entidades do jogo.
 *
 * 1. Cada ator possui um valor para ataque (ATK) e um valor de defesa (DEF)
 */

public abstract class Actor {
    /* Atributos */
    protected int atk, def;

    /* Getters and Setters */
    public int getAtk() { return this.atk; }

    public int getDef() { return this.def; }

    public void setAtk(int atk) { 
        if (atk >= 0)
            this.atk = atk;
        else this.atk = 0;
    }

    public void setDef(int def) { 
        if (def >= 0)
            this.def = def; 
        else this.def = 0;
    }

    /* Metodos */
    public void attack(Actor adversary) {
        adversary.setDef(adversary.getDef() - this.atk);
    }

    public void block(int dmg) {
        this.setDef(this.def - dmg);
    }

    public void cure(int hp) {
        this.setDef(this.def + hp);
    }
}

