import java.util.Iterator;
import java.util.Scanner;
import java.util.ArrayList;
import java.util.Random;

/**
 * Menor unidade acessivel do tabuleiro.
 *
 * 4. Cada setor possui uma posição no tabuleiro [x,y] e quatro lados que podem ser
 * portas ou paredes. Por exemplo, na Figura 1, a posição C corresponde ao Setor
 * [3,3], que possui quatro portas (*) e dois jogadores: P1 (jogador simples) e P2
 * (jogador de suporte)
 * 
 * 6. Cada setor pode possuir até 4 portas para que um jogador se movimente entre os
 * setores. Estas portas serão definidas pelo programador durante o desenvolvimento,
 * ou seja, em tempo de compilação.
 *
 * 9. Podem existir 3 tipos de setores:
 *     a. Setor normal
 *     b. Setor oculto
 *     c. Setor privado:
 */

public abstract class Sector {
    /* Atributos */
    private static int MAX_ENEMIES = 3; 

    protected int wallPattern;
    protected ArrayList<Enemy> mobs;

    /* Construtores */
    public Sector(int wallPattern) {
        this.wallPattern = wallPattern;
        this.mobs = new ArrayList<Enemy>(MAX_ENEMIES); // Especificacao do projeto: Questionavel...
    }

    /* Getters and Setters */

        /* Getters */
    public int getWallPattern() { return this.wallPattern; }
    
    public ArrayList<Enemy> getMobs() { return this.mobs; }

        /* Setters */
    public void setWallPattern(int wallPattern) {
        this.wallPattern = wallPattern;
    }

    public void setMobs(ArrayList<Enemy> mobs) { this.mobs = mobs; }

    /* Metodos */
    public boolean checkCombat() {
        return this.getMobs().isEmpty() ? false : true;
    }

    public void addEnemy() {
        Enemy aux;
        this.getMobs().add(aux = new Enemy());
    }

    public void generateEnemies() {
        Random dice = new Random();
        int nEnemies, i;

        nEnemies = dice.nextInt(2) + 1; // [0..2] + 1 inimigos

        for (i = 0; i < nEnemies; i++) 
            this.addEnemy();
    }

    public void clearEnemys() {
        this.getMobs().clear();
    }

    /* Interacao com Player */
    public void attack(Player antivirus) {
        Enemy mob;
        Iterator<Enemy> i = this.mobs.iterator();

        while (i.hasNext()) {
            mob = i.next(); 
            mob.attack(antivirus);
        }
    }

    public void defend(int atk) {
        ArrayList<Enemy> infection = this.mobs;

        if (!infection.isEmpty()) {
            Enemy aux = infection.get(0);        
            aux.block(atk);

            if (aux.getDef() == 0)  //Inimigo foi Morto
                infection.remove(0);
        }
    }

    public void scan(Player antivirus) {
        Random dice = new Random();
        Enemy mob;

        int aux = dice.nextInt(6) + 1; //[1..6]

        switch (aux) {
            case 4: antivirus.cure(1); break;
            case 5: antivirus.cure(2); break;
            case 6:
                for (Iterator<Enemy> i = this.mobs.iterator(); i.hasNext();) {
                    mob = i.next(); mob.block(1); //Todos inimigos perdem 1 de def
                    if (mob.getDef() == 0)
                        i.remove();
                }
                break;
        }
    }
}

