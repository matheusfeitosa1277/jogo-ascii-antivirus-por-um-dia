/**
 * Interface grafica do programa.
 */

public interface Igrafica {
    void cleanCanvas();
    void drawMap();

    void drawMoveMenu(Player antivirus);
    void drawActionMenu(Player antivirus, Sector sec);
    void drawHealMenu(Player heroes[]);

    void drawInfection(Player antivirus);
    void drawVictory();
    void drawLost();
}

