/**
 * Jogador simples.
 *
 * 1. O jogador simples deve ter ATK=2 e DEF=6 (representado por 2/6)
 *
 * 3. O jogador simples não possui habilidades especiais,
 *
 * 6. O jogador simples pode executar as seguintes ações no jogo: movimentar, atacar e
 * procurar
 */

public class Attack extends Player {
    /* Atributos */
    private static int DEFAULT_ATK = 2, DEFAULT_DEF = 6;

    /* Construtores */
    public Attack() {
        super();

        this.setAtk(DEFAULT_ATK); this.setDef(DEFAULT_DEF);
    }

    /* Getters and Setters */
    @Override
    public void setDef(int def) {
        if (def > 0) {
            if (def <= DEFAULT_DEF) 
                this.def = def;
            else this.def = DEFAULT_DEF;
        } else this.def = 0;
    }
}

