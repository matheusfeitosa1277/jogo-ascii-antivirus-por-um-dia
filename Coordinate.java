/**
 * Armazena uma Coordenada Cartesiana discreta.
 */

public class Coordinate {
    /* Atributos */
    private int x, y;

    /* Construtores */
    public Coordinate(int x, int y) {
        this.setX(x); this.setY(y);
    }

    /* Getters and Setters */
    public int getX() { return this.x; }

    public int getY() { return this.y; }

    public void setX(int x) { this.x = x; }

    public void setY(int y) { this.y = y; }


    /* Metodos */
    public boolean equal(Coordinate pos) {
        if (this.getX() == pos.getX() && this.getY() == pos.getY())
            return true;
        return false;
    }

    public void addX(int i) { this.x += i;}

    public void addY(int i) { this.y += i;}

        /* Debug */
    public void println() {
        System.out.printf("%d %d\n", this.x, this.y);
    }
}

