import java.util.Scanner;

/**
 * O jogo é formado por dois tipos de jogadores: simples e de suporte.
 *
 * 1. Cada jogador possui um valor para ataque (ATK) e um valor de defesa (DEF), sendo
 * que o poder de ataque e defesa dos jogadores são fixos.
 *
 * 2. Cada jogador ocupa uma posição no tabuleiro (POS), com duas coordenadas (X,Y)
 */

public abstract class Player extends Actor {
    /* Atributos */
    private static final int DEFAULT_POS = 2;

    protected Coordinate position;

    /* Construtores */
    public Player() {
        this.setPosition(DEFAULT_POS, DEFAULT_POS);
    }

    /* Getters and Setters */
    public Coordinate getPosition() { return this.position; }

    public void setPosition(Coordinate position) { this.position = position; }

    public void setPosition(int x, int y) { this.position = new Coordinate(x, y); }

    /* Metodos */
    public void attack(Sector sec) {
        sec.defend(this.atk);
    }

    public void search(Sector sec) {
        if (!(sec instanceof PrivateSec))
            sec.scan(this);
    }

    public void move(Sector sec) {
        Scanner input = new Scanner(System.in);
        String dir = input.next();

        Coordinate aux = this.position;
        int wall = sec.getWallPattern();

        if (dir.equals("U") && ((wall % 5) != 0))
                aux.addY(-1);
        else if (dir.equals("D") && ((wall % 2) != 0))
                aux.addY(1);
        else if (dir.equals("L") && ((wall % 7) != 0))
                aux.addX(-1);
        else if (dir.equals("R") && ((wall % 3) != 0))
                aux.addX(1);
    }

        /* Debug */
    public void printPos() {
        this.getPosition().println();
    }

}

