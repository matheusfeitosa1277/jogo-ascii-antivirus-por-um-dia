import java.util.Scanner;

/**
 * Jogador suporte.
 *
 * 1. O jogador de suporte deve ter ATK=1 e DEF=7 (representado por 1/7). 
 * 
 * 3. O jogador de suporte, possui a capacidade de recuperar 2 pontos de sua defesa ou da defesa 
 * de outro jogador que esteja no mesmo setor.
 *
 * 4. O jogador de suporte deve sempre possuir ataque menor que o jogador simples.
 *
 * 5. O jogador de suporte não pode existir sem um jogador simples
 *
 * 7. O jogador de suporte pode executar as seguintes ações no jogo: movimentar, atacar,
 * procurar e recuperar defesa. 
 */

public class Support extends Player {
    /* Atributos */
    private static int DEFAULT_ATK = 1, DEFAULT_DEF = 7, HEAL = 2;

    /* Construtores */
    public Support() {
        super();

        this.setAtk(DEFAULT_ATK); this.setDef(DEFAULT_DEF);
    }

    /* Getters and Setters */
    @Override
    public void setDef(int def) {
        if (def > 0) {
            if (def <= DEFAULT_DEF) 
                this.def = def;
            else this.def = DEFAULT_DEF;
        } else this.def = 0;
    }

    /* Metodos */
    public void heal(Player heroes[]) {
        Coordinate support = this.getPosition(), player = heroes[0].getPosition();
        Scanner input = new Scanner(System.in);

        switch (input.next()) {
            case "A": 
                if (support.equal(player)) //Mesmo Setor
                    heroes[0].cure(HEAL);
                break;
            case "S": heroes[1].cure(HEAL); break;
        }
    }
}

