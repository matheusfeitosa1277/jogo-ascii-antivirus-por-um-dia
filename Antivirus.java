import java.util.Scanner;
import java.util.Random;

/**
 * O jogo deve funcionar em sistema de turnos, alternando entre jogadores e inimigos.
 *
 * 1. Os jogadores possuem até 25 ciclos para encontrar a fonte de infecção do vírus (X).
 * Cada ciclo é formado pelos turnos do P1, P2 e inimigos
 *
 * 2. Os jogadores iniciam no centro do tabuleiro (Turno 0) e devem selecionar uma das
 * portas do setor inicial (C) para se movimentar para um novo setor. O primeiro a jogar
 * é sempre o jogador simples (P1), seguido pelo jogador de suporte (P2).
 *
 * 3. Quando um jogador se movimentar para um setor ainda não visitado, duas ações
 * devem ocorrer
 *     a. As portas do tabuleiro do setor visitado devem se tornar visíveis.
 *     b. Podem ser criados de 1 a 3 inimigos no setor, com ATK igual a DEF e no
 *     intervalo de 1 a 3. Tanto o número de inimigos, quanto o seu ATK e DEF
 *     devem ser gerados aleatoriamente.
 * 4. Após entrar no setor e gerar os inimigos, o turno inicia com o jogador P1, que pode
 * executar duas ações*:
 *     a. Atacar um inimigo. Por exemplo, suponha que P1 com ATK=2 ataque o
 *     inimigo 2 (3/3). Esse ataque reduz em 2 a DEF do inimigo, ou seja, o inimigo
 *     2 ficará com ATK/DEF igual a 3/1.
 *     b. Procurar no setor por itens. Esta procura consiste em sortear um número
 *     aleatório entre 1 e 6.
 *         i. Para números entre 1 e 3, nada é encontrado no setor.
 *         ii. Para número igual a 4, o jogador ganha 1 de DEF.
 *         iii. Para número igual a 5, o jogador ganha 2 de DEF.
 *         iv. Para número igual a 6, todos os inimigos do setor perdem 1 de DEF.
 *     *Cada opção acima (a,b) representa uma única ação, ou seja, o jogador pode:
 *     atacar duas vezes, atacar e procurar ou procurar duas vezes.
 *
 * 5. Após as duas ações do jogador P1, o turno do jogador P2 é iniciado. No seu turno,
 * P2 pode realizar as mesmas ações descritas no item 4. Além disso, o jogador P2
 * pode optar por usar sua habilidade especial (recuperar defesa) no lugar das ações
 * de atacar e procurar. Essa habilidade consiste em adicionar 2 a DEF dele ou do
 * jogador P1, desde que P1 esteja no mesmo setor que P2.
 *
 * 6. Após finalizar os turnos de P1 e P2, os inimigos que estiverem vivos iniciam seu
 * turno de ataque (única ação permitida). Cada inimigo ataca cada jogador uma única
 * vez (somente os jogadores que estiverem no mesmo setor). Por exemplo, suponha
 * que o inimigo 3 (2/2) ataque P1 (2/6) e P2 (1/7). Para cada ataque, um número
 * aleatório entre 1 e 6 deve ser gerado. Se o número for par, o ataque é realizado;
 *caso contrário, nada acontece. No caso do ataque ser destinado a P1, ele ficará com
 * 2/4, ou seja, a DEF do jogador atacado é reduzida pelo ATK do inimigo atacante. Em
 * seguida, o inimigo 3 deve atacar P2 usando esse mesmo raciocínio. 
 *
 * 7. Após todos os inimigos do setor finalizarem seu ataque, um novo ciclo é iniciado.
 *
 * 8. Os jogadores P1 e P2 só podem se movimentar para outro setor, quando não
 * existirem mais inimigos no setor em que eles se encontram. A Figura 3 mostra um
 * exemplo em que P1 se movimentou para o Setor [2,4] e P2 para o Setor [3,5], onde
 * as portas ficaram visíveis e os inimigos foram gerados.
 *
 * 9. Setores já visitados não geram novos inimigos, porém ao passar por este setor, o
 * jogador pode usar o seu turno para procurar itens (permitido aos jogadores P1 e
 * P2) ou recuperar defesa (permitido apenas ao jogador P2).
 */

public class Antivirus {
    private static final int ATK = 0, SPT = 1;
    private static int ROUND = 25, N_PLAYERS = 2, TURN_STATES = 3, N_ROUND = 2;

    public static void main(String args[]) {
        boolean victory = false;
        int state, nPlayer, round;

        Scanner input = new Scanner(System.in);

        Attack  p1 = new Attack(); Support aux, p2 = new Support();
        Player heroes[] = {p1, p2};

        SectorMap secMap = new SectorMap(); AsciiMap ascii = new AsciiMap();

        Coordinate pos; Sector sec; Player antivirus; //Variaveis auxiliares
        
        ascii.cleanCanvas();

        rounds:
        do {
            for (state = 0; state < TURN_STATES; state++)           // Fases do turno
                for (nPlayer = ATK; nPlayer < N_PLAYERS; nPlayer++) { // Para cada jogador
                    antivirus = heroes[nPlayer]; pos = antivirus.getPosition(); sec = secMap.getSec(pos);

                    if (antivirus.getDef() > 0)
                    switch(state) {
                        case 0: //Pre-combate
                            if (!sec.checkCombat()) { // Setor sem combate
                                ascii.drawMap(); ascii.drawMoveMenu(antivirus);

                                ascii.cleanPosition(antivirus); 
                                antivirus.move(sec); ascii.updatePosition(antivirus);

                                ascii.revealSector(secMap, pos); ascii.updateMiniPos(antivirus);
                                ascii.cleanCanvas();

                                if (secMap.getInfection().equal(pos)){ // Moveu para o centro da infeccao
                                    victory = true;
                                    ascii.drawInfection(antivirus);
                                    break rounds;
                                }
                            }
                            break;

                        case 1: //Fase Combate
                            for (round = 0; round < N_ROUND; round++) { //2 Acoes por jogador
                                ascii.updateHeroes(heroes, secMap);

                                ascii.drawMap(); ascii.drawActionMenu(antivirus, sec);

                                switch (input.next()) {
                                    case "A": antivirus.attack(sec); break;
                                    case "S": antivirus.search(sec); break;
                                    case "H":
                                        if (antivirus instanceof Support) {
                                            ascii.drawHealMenu(heroes);
                                            aux = (Support) antivirus; aux.heal(heroes);
                                        }
                                }

                                ascii.updateMiniHeroes(heroes);
                                ascii.updateHeroes(heroes, secMap);

                                ascii.cleanCanvas();
                            }
                            break;

                        case 2: //Turno dos Inimigos
                            if (sec.checkCombat()) 
                                sec.attack(antivirus);
                            
                            ascii.updateMiniStats(antivirus);

                            if (heroes[ATK].getDef() == 0) break rounds;
                            break;
                    }
                }
        } while (ROUND > 0) ;

        ascii.drawMap();

        if (victory)
            ascii.drawVictory();
        else ascii.drawLost();
    }
}

